document.addEventListener('DOMContentLoaded', function() {
    const input = document.getElementById('input_text');
    const preview = document.getElementById('prev_text');

    input.addEventListener('input', function() {
        const newText = escapeHtml(input.value);
        const formattedText = bb_func(newText);
        preview.innerHTML = formattedText;
    });

    function bb_func(text) {
        return text
            .replace(/\[b\](.*?)\[\/b\]/g, '<b>$1</b>')
            .replace(/\[i\](.*?)\[\/i\]/g, '<i>$1</i>')
            .replace(/\[s\](.*?)\[\/s\]/g, '<s>$1</s>')
            .replace(/\[color=([^\]]+)\](.*?)\[\/color\]/g, '<span style="color:$1">$2</span>');
    }

    function escapeHtml(unsafe) {
        return unsafe
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;");
    }
});
